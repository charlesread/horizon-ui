FROM node:10.16-jessie

#Get environment updated
RUN apt-get update && apt-get upgrade -yf

#Application
ADD src/ /app
RUN cd /app
WORKDIR /app
RUN rm -rf node_modules
RUN npm i

EXPOSE 8443
CMD ["node", "index.js"]