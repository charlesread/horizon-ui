'use strict'

const config = {}

config.context = 'kubernetes-admin@kubernetes'

config.image = 'registry.gitlab.com/charlesread/horizon-ui'

config.deployment = {
  name: 'registry.gitlab.com/charlesread/horizon-ui',
  containerName: 'registry.gitlab.com/charlesread/horizon-ui'
}

module.exports = config

/*

Sample deployment.yaml

apiVersion: apps/v1
kind: Deployment
metadata:
  name: k8s-nginx-demo    #   <----------- use this for deployment.name
  labels:
    app: k8s-nginx-demo
spec:
  replicas: 1
  selector:
    matchLabels:
      app: k8s-nginx-demo
  template:
    metadata:
      labels:
        app: k8s-nginx-demo
    spec:
      containers:
        - name: k8s-nginx-demo  #   <----------------- use this for deployment.containerName
          image: registry.gitlab.com/clayton-state-university/k8s-nginx-demo
          ports:
            - containerPort: 80
      imagePullSecrets:
        - name: gitlab


 */